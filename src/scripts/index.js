import { AddNew } from "./components/AddNew.js";
import { RenderImage } from "./components/RenderImage.js";

document.getElementById("adicionar").addEventListener("click", function (e) {
    e.preventDefault();
    new AddNew();
});

window.onload = function () {
    new RenderImage();
};
