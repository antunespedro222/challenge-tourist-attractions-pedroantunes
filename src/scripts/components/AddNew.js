export class AddNew {
    constructor() {
        var imagem = document.querySelector("#image-box img").src;
        var titulo = document.getElementById("input-titulo").value;
        var descricao = document.getElementById("input-descricao").value;
        var posicao = document.getElementById("pontos-turisticos");

        var montaBloco =
            '<div class="bloco-turistico">' +
            "<img src=" +
            imagem +
            " />" +
            '<div class="infos">' +
            "<h3>" +
            titulo +
            "</h3>" +
            "<p>" +
            descricao +
            "</p>" +
            "</div>" +
            "</div>";

        if (titulo === "" || descricao === "" || imagem === window.location.href) {
            window.alert("Insira todos os dados");
        } else {
            posicao.innerHTML += montaBloco;
        }
    }
}
