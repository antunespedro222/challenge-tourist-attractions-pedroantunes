export class RenderImage{

    constructor(){
        var imagem = document.getElementById('imagem');
        var showImage = document.getElementById('image-box');
    
    
        imagem.addEventListener('change', function() {
    
            var file = imagem.files[0];
            var imageType = /image.*/;
    
            console.log(file);
            
            // console.log(file.type.match(imageType));
    
            if (file.type.match(imageType)) { 
                
                var reader = new FileReader();
    
                reader.readAsDataURL(file);	
    
                reader.onload = function() {
                    showImage.innerHTML = "";
    
                    var img = new Image();
                    img.src = reader.result;
    
                    showImage.appendChild(img);
                }
                
            } else {
                showImage.innerHTML = "File not supported!"
            }
        });
    }
    
}
