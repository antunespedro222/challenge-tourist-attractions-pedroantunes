const { src, dest, watch } = require("gulp");
const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
var uglify = require("gulp-uglify");
var buffer = require("vinyl-buffer");
const webp = require("gulp-webp");

function styles() {
    return gulp
        .src("src/styles/main.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(gulp.dest("dist"));
}

function scripts() {
    return browserify("src/scripts/index.js")
        .transform(
            babelify.configure({
                presets: ["@babel/preset-env"],
            })
        )
        .bundle()
        .pipe(source("bundle.js"))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(dest("dist"));

    // return gulp.src("src/scripts/*.js").pipe(concat("bundle.js")).pipe(gulp.dest("dist"));
}

function images() {
    gulp.src("src/images/*").pipe(webp()).pipe(gulp.dest("dist/images"));
}

function sentinel() {
    watch("src/styles/**/*.scss", { ignoreInitial: false }, styles);
    watch("src/scripts/**/*.js", { ignoreInitial: false }, scripts);
    watch("src/images/*", { ignoreInitial: false }, images);
}

exports.sentinel = sentinel;
